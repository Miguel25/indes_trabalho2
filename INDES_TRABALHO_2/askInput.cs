﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace INDES_TRABALHO_2
{
    public partial class askInput : Form
    {
        public string Url { get; set; }
        public List<string> LocalVideosPaths { get; set; } 

        public askInput()
        {
            InitializeComponent();
            label1.Text = "";
            label2.Text = "Insira o endereço URL do video (youtube...)";
            label3.Text = "Selecione um ficheiro local";
            label4.Text = "Selecione o tipo de ficheiro que pretende inserir";
            button1.Text = "Cancelar";
            button2.Text = "Adicionar";
            button3.Text = "Selecionar Ficheiro";
            groupBox1.Text = "Input";
            radioButton1.Text = "Online";
            radioButton2.Text = "Local";
            textBox1.Enabled = false;
            button3.Enabled = false;
            LocalVideosPaths = new List<string>();
        }

        public Type getInputType() {
            if (radioButton1.Checked) {
                return Type.online;
            } else if (radioButton2.Checked) {
                return Type.local;
            }else {
                return Type.unavailable;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if( (radioButton1.Checked && !radioButton2.Checked) || (!radioButton1.Checked && radioButton2.Checked))
            {
                if (radioButton1.Checked)
                {
                    if (String.IsNullOrEmpty(Url))
                    {
                        MessageBox.Show("Por favor introduza o Url do vídeo");
                    }
                    else
                    {
                        Close();
                    }

                }
                else if (radioButton2.Checked)
                {
                    if (LocalVideosPaths.Count == 0)
                    {
                        MessageBox.Show("Por favor selecione pelo menos um vídeo");
                    }
                    else
                    {
                        Close();
                    }
                }
            }
            else
            {
                MessageBox.Show("Por favor selecione a origem do vídeo");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var FD = new System.Windows.Forms.OpenFileDialog();
            FD.Filter = "Video Files|*.mp4";
            FD.Multiselect = true;
            if (FD.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                LocalVideosPaths = new List<string>(FD.FileNames);
            }
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            textBox1.Enabled = true;
            button3.Enabled = false;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            Url = textBox1.Text;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            button3.Enabled = true;
            textBox1.Enabled = false;
        }
    }
}
