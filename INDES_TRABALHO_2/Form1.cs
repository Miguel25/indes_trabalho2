﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Net;
using System.Reflection;
using System.Web;
using System.Windows.Forms;
using System.Security.Permissions;
using AForge.Video;

namespace INDES_TRABALHO_2
{
    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    [System.Runtime.InteropServices.ComVisibleAttribute(true)]
    public partial class Form1 : Form
    {
        List <Button> controlBtns;
        List <Button> listBtns;
        List <ListBox> listBoxs;
        Webcam webcam;
        Image techDif, statics;
        string fullPath;
        MJPEGStream stream;

        public Form1()
        {
            InitializeComponent();

            string exeFile = (new Uri(Assembly.GetEntryAssembly().CodeBase)).AbsolutePath;
            string exeDir = Path.GetDirectoryName(exeFile);
            fullPath = Path.Combine(exeDir, "..\\..\\res\\");
            try {
                techDif = Image.FromFile(fullPath + "hqdefault.jpg");
                statics = Image.FromFile(fullPath + "transferir.jpg");
            }
            catch (FileNotFoundException e)
            {
                MessageBox.Show(fullPath);
            }
            groupBox1.Text = "Live Feed";
            groupBox2.Text = "Options";
            groupBox3.Text = "Preview";
            label1.Text = "Camera";
            label2.Text = "Video 1";
            label3.Text = "Video 2";
            label4.Text = "Video 3";
            label5.Text = "Camera Input";
            label6.Text = "Video 1 Input";
            label7.Text = "Video 2 Input";
            label8.Text = "Video 3 Input";
            label9.Text = "Play Camera";
            label10.Text = "Play Video 1";
            label12.Text = "Play Video 2";
            label13.Text = "Play Video 3";
            label11.Text = "Tecnichal Dificulties";
            label14.Text = "Shup Off";
            button11.Text = "Play Preview";
            button12.Text = "Play Preview";
            button13.Text = "Play Preview";
            button14.Text = "Play Preview";

            listBoxs = new List<ListBox>
            {
                listBox1, listBox2, listBox3, listBox4
            };

            controlBtns = new List<Button>
            {
                button5, button6, button7, button8, button9, button10
            };

            listBtns = new List<Button>
            {
                button1, button2, button3, button4
            };

            foreach(ListBox l in listBoxs)
            {
                l.DisplayMember = "Name";
                l.AllowDrop = true;
                l.BackColor = Color.LightGray;
            }

            foreach (Button btn in controlBtns)
            {
                btn.Text = "";
                btn.BackColor = Color.Red;
            }

            webBrowser4.Visible = false;
            webBrowser5.Visible = false;
            webBrowser6.Visible = false;
            webBrowser7.Visible = false;
            pictureBox2.Visible = false;

            webBrowser1.DocumentText = generateEmbededPreview();
            webBrowser2.DocumentText = generateEmbededPreview();
            webBrowser3.DocumentText = generateEmbededPreview();
            webBrowser4.DocumentText = generateEmbededMain();
            webBrowser5.DocumentText = generateEmbededMain();
            webBrowser6.DocumentText = generateEmbededMain();

            webBrowser1.ObjectForScripting = this;

            pictureBox1.BackColor = Color.Black;

            button10_Click( new object(), new EventArgs());
            foreach (Button btn in listBtns) {
                btn.Text = "Add Video Feed";
            }
            pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
            webcam = new Webcam();
            stream = new MJPEGStream("");
            Video v = new Video("Webcam", "", Type.local);
            addVideoOption(0, v);
        }
        
        public void Test(String str)
        {
            MessageBox.Show(str + "", "client code");
            switch (str)
            {
                case "2":
                    MessageBox.Show(listBox2.SelectedIndex + " " + listBox2.Items.Count, "client code");
                    if (listBox2.SelectedIndex < listBox2.Items.Count-1)
                    {
                        listBox2.SelectedIndex = listBox2.SelectedIndex+1;
                        button12_Click(new object(), new EventArgs());
                    }
                    break;
                case "3":
                    MessageBox.Show(listBox3.SelectedIndex + " " + listBox3.Items.Count, "client code");
                    if (listBox3.SelectedIndex < listBox3.Items.Count - 1)
                    {
                        listBox3.SelectedIndex = listBox3.SelectedIndex + 1;
                        button13_Click(new object(), new EventArgs());
                    }
                    break;
                case "4":
                    MessageBox.Show(listBox4.SelectedIndex + " " + listBox4.Items.Count, "client code");
                    if (listBox4.SelectedIndex < listBox4.Items.Count - 1)
                    {
                        listBox4.SelectedIndex = listBox4.SelectedIndex + 1;
                        button14_Click(new object(), new EventArgs());
                    }
                    break;
                default:
                    break;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int index = 0;
            askInputIpCam r = new askInputIpCam();
            r.ShowDialog();
            Video v = new Video(r.CameraName, r.IP, Type.online);
            addVideoOption(index, v);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            addOptionsToListBox(listBox2);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            addOptionsToListBox(listBox3);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            addOptionsToListBox(listBox4);
        }

        private void addOptionsToListBox(ListBox lb)
        {
            askInput r = new askInput();
            r.ShowDialog();
            if (r.getInputType().Equals(Type.online))
            {
                Video v = new Video(GetTitle(r.Url), r.Url, r.getInputType());
                lb.Items.Add(v);
            }
            else
            {
                foreach (string s in r.LocalVideosPaths)
                {
                    Video v = new Video(Path.GetFileName(s), s, r.getInputType());
                    lb.Items.Add(v);
                }
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            int index = 0;
            colorControlButtons(index);
            pictureBox2.Visible = true;
            webBrowser4.Visible = false;
            webBrowser5.Visible = false;
            webBrowser6.Visible = false;
            webBrowser7.Visible = false;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            int index = 1;
            colorControlButtons(index);
            Video v = (Video)listBox2.SelectedItem;
            pictureBox2.Visible = false;
            webBrowser4.Visible = true;
            webBrowser5.Visible = false;
            webBrowser6.Visible = false;
            webBrowser7.Visible = false;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            int index = 2;
            colorControlButtons(index);
            pictureBox2.Visible = false;
            webBrowser4.Visible = false;
            webBrowser5.Visible = false;
            webBrowser6.Visible = false;
            webBrowser7.Visible = true;
            webBrowser7.DocumentText = generateEmbededMain(fullPath + "hqdefault.jpg");
        }

        private void button8_Click(object sender, EventArgs e)
        {
            int index = 3;
            colorControlButtons(index);
            Video v = (Video)listBox3.SelectedItem;
            pictureBox2.Visible = false;
            webBrowser4.Visible = false;
            webBrowser5.Visible = true;
            webBrowser6.Visible = false;
            webBrowser7.Visible = false;
        }

        private void button9_Click(object sender, EventArgs e)
        {
            int index = 4;
            colorControlButtons(index);
            pictureBox2.Visible = false;
            webBrowser4.Visible = false;
            webBrowser5.Visible = false;
            webBrowser6.Visible = true;
            webBrowser7.Visible = false;
            
        }

        private void button10_Click(object sender, EventArgs e)
        {
            int index = 5;
            colorControlButtons(index);
            pictureBox2.Visible = false;
            webBrowser4.Visible = false;
            webBrowser5.Visible = false;
            webBrowser6.Visible = false;
            webBrowser7.Visible = true;
            webBrowser7.DocumentText = generateEmbededMain(fullPath + "transferir.jpg");
        }

        private void button12_Click(object sender, EventArgs e)
        {
            playVideo(listBox2, webBrowser1, webBrowser4, '2');
        }

        private void button13_Click(object sender, EventArgs e)
        {
            playVideo(listBox3, webBrowser2, webBrowser5, '3');
        }

        private void button14_Click(object sender, EventArgs e)
        {
            playVideo(listBox4, webBrowser3, webBrowser6, '4');
        }

        private void playVideo(ListBox lb, WebBrowser wbPrev, WebBrowser wbMain, Char index)
        {
            Video v = (Video)lb.SelectedItem;
            if (v != null)
            {
                wbPrev.DocumentText = generateEmbededPreview(v,index);
                wbMain.DocumentText = generateEmbededMain(v);
            }
            else
            {
                MessageBox.Show("Selecione um vídeo");
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Video v = (Video)listBox1.SelectedItem;
            if (v.Type.Equals(Type.local))
            {
                if (stream.IsRunning)
                {
                    stream.Stop();
                }
                if (!webcam.isWebcamRunning())
                {
                    webcam.InitializeWebCam(ref pictureBox1, ref pictureBox2);
                    webcam.Start();
                }
            }
            else
            {
                if (webcam.isWebcamRunning())
                {
                    webcam.Stop();
                }
                if (!stream.IsRunning)
                {
                    stream = new MJPEGStream(v.Url);
                    stream.NewFrame += stream_NewFrame;
                    stream.Start();
                }
                else
                {
                    stream.Source = v.Url;
                    stream.NewFrame += stream_NewFrame;
                    stream.Start();
                }
            }
        }

        private void colorControlButtons(int index)
        {
            foreach (Button btn in controlBtns)
            {
                btn.Text = "";
                btn.BackColor = Color.Red;
            }
            controlBtns[index].BackColor = Color.Green;
        }

        private string generateEmbededPreview(Video v, Char lb)
        {
            if (v.Type.Equals(Type.online))
            {
                var embed = "<html><head>" +
                "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=Edge\"/>" +
                "</head><body bgcolor=\"#000000\" style=\"margin:0;\">" +
                "<script type='text/javascript' src='http://www.youtube.com/player_api'></script>"+
                "<iframe class=\"ytplayer\" width=\"225\" height=\"103\" src=\"{0}\"" +
                "frameborder = \"0\" allow = \"autoplay\"></iframe>" +
                "<script type=\"text/javascript\">" +
                       "var player;"+
                            "function onYouTubeIframeAPIReady(){"+
                                "player = new YT.Player('player', {"+
                        "events:{'onStateChange': function(evt){"+
                         " if (evt.data == 0){window.external.Test('Video finished!!');}}}});}"+
                "</script> " +
                "</body></html>";
                string url = v.Url.Replace("watch?v=", "embed/");
                return string.Format(embed, url + "?autoplay=1&controls=0&mute=1&showinfo=0");
            }
            else
            {
                string html = String.Format("<html>" +
                                                "<head>" +
                                                    "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=Edge\"/>" +
                                                    "<body bgcolor=\"#000000\" style=\"margin:0;\">" +
                                                        "<video autoplay onEnded=\"window.external.Test("+ lb + ")\" muted width=\"225\" height=\"103\">" +
                                                            "<source src =\"{0}\" type = \"video/mp4\" >" +
                                                        "</video>" +
                                                    "</body>" +
                                                    "</head>" +
                                            "</html>", v.Url);
                return html;
            }
        }

        private string generateEmbededPreview()
        {
            return String.Format("<html>" +
                                                "<head>" +
                                                    "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=Edge\"/>" +
                                                    "<body bgcolor=\"#000000\" style=\"margin:0;\">" +
                                                    "</body>" +
                                                    "</head>" +
                                            "</html>");
        }
        

        private string generateEmbededMain(Video v)
        {
            if (v.Type.Equals(Type.online))
            {
                var embed = "<html><head>" +
                "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=Edge\"/>" +
                "</head><body bgcolor=\"#000000\" style=\"margin:0;\">" +
                "<iframe width=\"365\" height=\"280\" src=\"{0}\"" +
                "frameborder = \"0\" allow = \"autoplay\"></iframe>" +
                "</body></html>";
                string url = v.Url.Replace("watch?v=", "embed/");
                return string.Format(embed, url + "?autoplay=1&controls=0&mute=1&showinfo=0");
            }
            else
            {
                string html = String.Format("<html>" +
                                                "<head>" +
                                                    "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=Edge\"/>" +
                                                    "<body bgcolor=\"#000000\" style=\"margin:0;\">" +
                                                        "<video autoplay muted width=\"365\" height=\"280\">" +
                                                            "<source src =\"{0}\" type = \"video/mp4\" >" +
                                                        "</video>" +
                                                    "</body>" +
                                                    "</head>" +
                                            "</html>", v.Url);
                return html;
            }
        }
        private string generateEmbededMain(string path)
        {
            return String.Format("<html>" +
                                                "<head>" +
                                                    "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=Edge\"/>" +
                                                    "<body bgcolor=\"#000000\" style=\"margin:0;\">" +
                                                        "<img width=\"365\" height=\"280\" src =\"{0}\" >" +
                                                        "</img>" +
                                                    "</body>" +
                                                    "</head>" +
                                            "</html>", path);
        }

        private string generateEmbededMain()
        {
            return String.Format("<html>" +
                                                "<head>" +
                                                    "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=Edge\"/>" +
                                                    "<body bgcolor=\"#000000\" style=\"margin:0;\">" +
                                                    "</body>" +
                                                    "</head>" +
                                            "</html>");
        }

        public static string GetTitle(string url)
        {
            if (url.Contains("youtube"))
            {
                var api = $"http://youtube.com/get_video_info?video_id={GetArgs(url, "v", '?')}";
                return GetArgs(new WebClient().DownloadString(api), "title", '&');
            }
            return "Video";
        }

        private static string GetArgs(string args, string key, char query)
        {
            var iqs = args.IndexOf(query);
            return iqs == -1
                ? string.Empty
                : HttpUtility.ParseQueryString(iqs < args.Length - 1
                    ? args.Substring(iqs + 1) : string.Empty)[key];
        }

        private void listBox2_MouseDown(object sender, MouseEventArgs e)
        {
            int ix = listBox2.IndexFromPoint(e.Location);
            if (ix != -1)
            {
                listBox2.DoDragDrop(ix.ToString(), DragDropEffects.Move);
            }
        }

        private void listBox2_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void listBox2_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.Text))
            {
                int dix = Convert.ToInt32(e.Data.GetData(DataFormats.Text));//changed this line
                int ix = listBox2.IndexFromPoint(listBox2.PointToClient(new Point(e.X, e.Y)));
                if (ix != -1)
                {
                    object obj = listBox2.Items[dix];
                    listBox2.Items.Remove(obj);
                    listBox2.Items.Insert(ix, obj);
                }

            }
        }

        private void listBox3_MouseDown(object sender, MouseEventArgs e)
        {
            int ix = listBox3.IndexFromPoint(e.Location);
            if (ix != -1)
            {
                listBox3.DoDragDrop(ix.ToString(), DragDropEffects.Move);
            }
        }

        private void listBox3_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void listBox3_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.Text))
            {
                int dix = Convert.ToInt32(e.Data.GetData(DataFormats.Text));//changed this line
                int ix = listBox3.IndexFromPoint(listBox3.PointToClient(new Point(e.X, e.Y)));
                if (ix != -1)
                {
                    object obj = listBox3.Items[dix];
                    listBox3.Items.Remove(obj);
                    listBox3.Items.Insert(ix, obj);
                }

            }
        }

        private void listBox4_MouseDown(object sender, MouseEventArgs e)
        {
            int ix = listBox4.IndexFromPoint(e.Location);
            if (ix != -1)
            {
                listBox4.DoDragDrop(ix.ToString(), DragDropEffects.Move);
            }
        }

        private void listBox4_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void listBox4_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.Text))
            {
                int dix = Convert.ToInt32(e.Data.GetData(DataFormats.Text));//changed this line
                int ix = listBox4.IndexFromPoint(listBox4.PointToClient(new Point(e.X, e.Y)));
                if (ix != -1)
                {
                    object obj = listBox4.Items[dix];
                    listBox4.Items.Remove(obj);
                    listBox4.Items.Insert(ix, obj);
                }

            }
        }
    }
}
