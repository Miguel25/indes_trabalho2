﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INDES_TRABALHO_2
{
    public class Video
    {

        public string Name { get; set; }
        public string Url { get; set; }
        public Type Type { get; set; }

        public Video(string name, string url, Type type)
        {
            Name = name;
            Url = url;
            Type = type;
        }
    }

}
