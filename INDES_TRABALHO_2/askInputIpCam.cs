﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace INDES_TRABALHO_2
{
    public partial class askInputIpCam : Form
    {
        public string CameraName { get; set; }
        public string IP { get; set; }

        public askInputIpCam()
        {
            InitializeComponent();
            groupBox1.Text = "Input";
            label2.Text = "Camera's name";
            label3.Text = "IP";
            button1.Text = "Cancel";
            button2.Text = "Add";
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            CameraName = textBox1.Text;
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            IP = textBox2.Text;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrEmpty(CameraName) || string.IsNullOrEmpty(IP))
            {
                MessageBox.Show("Enter all the needed data");
            }
            else
            {
                Close();
            }
        }
    }
}
